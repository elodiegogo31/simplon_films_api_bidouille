<?php

namespace App\Repository;

use App\Entity\MoviesOrders;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MoviesOrders|null find($id, $lockMode = null, $lockVersion = null)
 * @method MoviesOrders|null findOneBy(array $criteria, array $orderBy = null)
 * @method MoviesOrders[]    findAll()
 * @method MoviesOrders[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoviesOrdersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MoviesOrders::class);
    }

    // /**
    //  * @return MoviesOrders[] Returns an array of MoviesOrders objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MoviesOrders
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
