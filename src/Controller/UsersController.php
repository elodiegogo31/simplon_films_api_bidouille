<?php

namespace App\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Users;
use App\Repository\UsersRepository;


class UsersController extends AbstractController
{
    /**
     * @Route("/", name="connection")
     */
    public function index()
    {
        return $this->render('users/index.html.twig', [
            'controller_name' => 'UsersController',
        ]);
    }
    /**
     * @Route("/register", name="register")
     */
    // public function register(Request $request, ObjectManager $manager)
    // {
    //     $user = new User();

    //     //attention redefinir les champs !!! 
    //     $form = $this -> createFormBuilder($user)
    //                   -> add('username', [
    //                       'attr' =>  [
    //                           'placeholder' =>'Entrez votre nom'
    //                       ]
    //                   ])
    //                   -> add('email', [
    //                       'attr' => [
    //                           'placeholder'=>'Entrez votre email'
    //                       ]
    //                   ])
    //                   -> add('password', [
    //                       'attr' => [
    //                           'placeholder'=>'Definissez votre mot de passe'
    //                       ]
    //                   ])
    //                   -> getForm();

    //     return $this->render('users/register.html.twig', [
    //         'formRegister' => $form -> createView(),
    //         'controller_name' => 'UsersController'
    //     ]);
    // }

    /**
     * @Route("/generate", name="generate")
     */
    public function generate()
    {
        // A COMPLETER SI LE TEMPS 
        return $this->render('users/index.html.twig', [
            'controller_name' => 'UsersController',
        ]);
    }

    public function disconnect()
    {
        // A COMPLETER 
        return $this->render('users/index.html.twig', [
            'controller_name' => 'UsersController',
        ]);
    }


}
