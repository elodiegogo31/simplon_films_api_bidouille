<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Movies;
use App\Form\MovieType;


class APIControlerController extends AbstractController
{
    /**
     * @Route("/query", name="api_call")
     */
    public function api_call(Request $request, ObjectManager $manager)
    {
        $movie_search= $request->request->get('search_movie');
        $movie_data= file_get_contents("http://www.omdbapi.com/?t=".$movie_search."&apikey=a935e396");
        $movie_data_decoded = json_decode($movie_data);
        var_dump($movie_data_decoded);

        $movie = new Movies();

        $movie->settitle($movie_data_decoded->Title);
        $movie->setresume($movie_data_decoded->Plot);
        $movie->setyear($movie_data_decoded->Year);
        $movie->setreleased($movie_data_decoded->Released);
        $movie->setruntime($movie_data_decoded->Runtime);
        $movie->setgenre($movie_data_decoded->Genre);
        $movie->setdirector($movie_data_decoded->Director);
        $movie->setlanguage($movie_data_decoded->Language);
        $movie->setcountry($movie_data_decoded->Country);
        $movie->setawards($movie_data_decoded->Awards);
        $movie->setposter($movie_data_decoded->Poster);
        $movie->settype($movie_data_decoded->Type);
        $movie->setDVD($movie_data_decoded->DVD);
        $movie->setProduction($movie_data_decoded->Production);
        $movie->setavailable(true);
      /*  
        $movie->settitle($movie_data_decoded[0]['Title']);
        $movie->setresume($movie_data_decoded[0]['Plot']);
        $movie->setyear($movie_data_decoded[0]['Year']);
        $movie->setreleased($movie_data_decoded[0]['Released']);
        $movie->setruntime($movie_data_decoded[0]['Runtime']);
        $movie->setgenre($movie_data_decoded[0]['Genre']);
        $movie->setdirector($movie_data_decoded[0]['Director']);
        $movie->setlanguage($movie_data_decoded[0]['Language']);
        $movie->setcountry($movie_data_decoded[0]['Country']);
        $movie->setawards($movie_data_decoded[0]['Awards']);
        $movie->setposter($movie_data_decoded[0]['Poster']);
        $movie->settype($movie_data_decoded[0]['Type']);
        $movie->setDVD($movie_data_decoded[0]['DVD']);
        $movie->setProduction($movie_data_decoded[0]['Production']);
        $movie->setavailable(true);
        */

        $form=$this->createForm(MovieType::class, $movie);

        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()){
        $manager->persist($movie);
        $manager->flush();
        }

        return $this->render('api_controler/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
