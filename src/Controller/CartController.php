<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart")
     */
    public function index()
    {
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }
     /**
     * @Route("/borrow", name="borrow")
     */
    public function borrow($id)
    {
        // A COMPLETER
        return $this->render('cart/index.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }

        

}
