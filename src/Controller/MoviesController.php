<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Movies;
use App\Form\MovieType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;

class MoviesController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    
     //lister totalité des films
    public function index()
    {
        $repo = $this->getDoctrine()->getRepository(Movies::class);

        $movies = $repo->findAll();

        return $this->render('movies/index.html.twig', [
            'movies' => $movies
        ]);
    }

      /**
     * @Route("/add", name="add_form")
     */

     //ajouter un film by admin
     public function add(Request $request, ObjectManager $manager)
     {
         $movie = new Movies();
        $form=$this->createForm(MovieType::class, $movie);

        $form->handleRequest($request);
        if($form->isSubmitted()&& $form->isValid()){
        $manager->persist($movie);
        $manager->flush();
        }


         return $this->render('movies/add_movie.html.twig', [
             'form' => $form->createView()
         ]);
     }

       /**
     * @Route("/remove", name="remove")
     */
     //retirer un film by admin (attention confirmation)
    public function remove()
    {
        // A COMPLETER 
        return $this->render('movies/index.html.twig', [
            'controller_name' => 'MoviesController',
        ]);
    }

      /**
     * @Route("/edit", name="edit")
     */

     //modifier un film SAUF titre 
     public function edit()
     {
         return $this->render('movies/index.html.twig', [
             'controller_name' => 'MoviesController',
         ]);
     }


       /**
     * @Route("/filter", name="filter")
     */
     //filter/classer les films 
    public function filter()
    {
        return $this->render('movies/index.html.twig', [
            'controller_name' => 'MoviesController',
        ]);
    }

   
}
