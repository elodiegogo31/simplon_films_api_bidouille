<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="orders")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MoviesOrders", mappedBy="orders")
     */
    private $moviesOrders;

    public function __construct()
    {
        $this->moviesOrders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getUserId(): ?Users
    {
        return $this->user_id;
    }

    public function setUserId(?Users $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * @return Collection|MoviesOrders[]
     */
    public function getMoviesOrders(): Collection
    {
        return $this->moviesOrders;
    }

    public function addMoviesOrder(MoviesOrders $moviesOrder): self
    {
        if (!$this->moviesOrders->contains($moviesOrder)) {
            $this->moviesOrders[] = $moviesOrder;
            $moviesOrder->setOrders($this);
        }

        return $this;
    }

    public function removeMoviesOrder(MoviesOrders $moviesOrder): self
    {
        if ($this->moviesOrders->contains($moviesOrder)) {
            $this->moviesOrders->removeElement($moviesOrder);
            // set the owning side to null (unless already changed)
            if ($moviesOrder->getOrders() === $this) {
                $moviesOrder->setOrders(null);
            }
        }

        return $this;
    }
}
